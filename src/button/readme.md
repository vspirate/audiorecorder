# Button design
In this folder contains files to improve the design of buttons

## Fixing button hover
At the moment Qt has a problem with hovering, but this command fixes that:
```cpp
    if (event->type() == QEvent::Enter) {
        button->setIcon(QIcon(btnhover));
    }

    if (event->type() == QEvent::Leave){
        button->setIcon(QIcon(btnstat));
    }
```

Delcaration
```cpp
    ButtonHoverWatcher * watcher = new ButtonHoverWatcher(this, ":/close_settings/CloseSettingsHover.png", ":/close_settings/CloseSettings.png");
    ui->CloseBtn->installEventFilter(watcher);
```